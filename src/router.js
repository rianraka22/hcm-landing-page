import { createWebHistory, createRouter } from "vue-router";

const routes =  [
    {
      path: "/",
      alias: "/download",
      name: "download",
      component: () => import("./components/DownloadPage")
    }
  ];

  const router = createRouter({
    history: createWebHistory(),
    routes,
  });
  
  export default router;