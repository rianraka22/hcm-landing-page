import axios from "axios";

export default axios.create({
    baseURL: process.env.VUE_APP_URL+"main",
    headers: {
        "Content-type": "application/json"
    },
});


// "Access-Control-Allow-Origin": "*",
// "Access-Control-Allow-Methods":"GET, POST, OPTIONS, PUT, PATCH, DELETE",
// "Access-Control-Allow-Headers":"Origin,Content-Type,X-Requested-With,Accept,Authorization"